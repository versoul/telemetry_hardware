# Железячная часть проекта Telemetry#

Программная находится здесь  [Telemetry](https://bitbucket.org/versoul/telemetry)

### Структура проекта ###

* Файл hardware.fzz содержит все компоненты соединения и разводку железа (для программы Fritzing)
* Файл Telemetry/Telemetry.ino прошивка для ардуино


![Alt text](https://bytebucket.org/versoul/telemetry_hardware/raw/4f0f42b3a66ce35635b2ac5f6ce4aec83e25dd98/demo.jpg?token=0a1ef4c93553a0e4480914d75ceca52720bcbc01)