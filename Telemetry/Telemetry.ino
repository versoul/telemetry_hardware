
#include <SoftwareSerial.h>
// MAC 00:12:05:16:99:52
SoftwareSerial btSerial(5, 6); // RX, TX

#define L_R 9
#define L_G 10
#define L_B 11

unsigned long now_time;

unsigned long detect_time;
bool wait_falling = false;

char incomingByte;  // входящие данные
char buf[50];

int dt = 100;
void led_green() {
  analogWrite(L_R, 0);
  analogWrite(L_G, 50);
  analogWrite(L_B, 0);
}
void led_off() {
  analogWrite(L_R, 0);
  analogWrite(L_G, 0);
  analogWrite(L_B, 0);
}

String waiting_list[10];
int waiting_list_size = 0;
unsigned long waiting_list_time;


void waiting_list_loop(){
  if(waiting_list_size > 0 && now_time-waiting_list_time > 1000){
    waiting_list_remove();
  }
  
}
void waiting_list_add(String time){
  if(waiting_list_size<10){
    waiting_list[waiting_list_size] = time;
    waiting_list_size += 1;
    
    Serial.println("add");
    waiting_list_send();
  }
}
void waiting_list_send(){
    btSerial.println(waiting_list[0]);
    Serial.println("sending");
    Serial.println(waiting_list[0]);
    waiting_list_time = now_time;
}
void waiting_list_remove(){
  waiting_list[waiting_list_size] = "";
  waiting_list[0] = waiting_list[1];
  waiting_list[1] = waiting_list[2];
  waiting_list[2] = waiting_list[3];
  waiting_list[3] = waiting_list[4];
  waiting_list[4] = waiting_list[5];
  waiting_list[5] = waiting_list[6];
  waiting_list[6] = waiting_list[7];
  waiting_list[7] = waiting_list[8];
  waiting_list[8] = waiting_list[9];
  waiting_list[9] = "";
  
  waiting_list_size -= 1;
  Serial.println("remove");
}


void setup() {
  btSerial.begin(9600);
  Serial.begin(9600);
  
  pinMode(L_R, OUTPUT);
  pinMode(L_G, OUTPUT);
  pinMode(L_B, OUTPUT);
  analogWrite(L_B, 0);
  analogWrite(L_G, 0);
  analogWrite(L_R, 0);
  
  attachInterrupt(0, detect_line, RISING);
  detect_time = 0;
}
void detect_line(){
  now_time = millis();
  Serial.println("----");
  Serial.println("func 1");
  
  if(now_time-detect_time > 150 && wait_falling == false){
    Serial.println("if 1");
    ltoa(now_time, buf, 10);  // 10 is the base value not the size - look up ltoa for avr
    //btSerial.print("L");
    Serial.println("--=L="+String(now_time)+"=-&");
    waiting_list_add("--=L="+String(now_time)+"=-&");
    led_off();
    detect_time = now_time;
    //wait_falling = true;
    attachInterrupt(0, reAttachInterrupt, FALLING);
  }
  else if(now_time-detect_time > 100 && wait_falling == true){
    Serial.println("if 222");
    detect_time = now_time;
    wait_falling = false;
  }
}
void reAttachInterrupt(){
  now_time = millis();
  Serial.println("----");
  Serial.println("func 2");
  if(now_time-detect_time > 150){
    Serial.println("if 2");
    detect_time = now_time;
    attachInterrupt(0, detect_line, RISING);
  }
}
void loop() {
  now_time = millis();
  
  if (btSerial.available() > 0) {  
    incomingByte = btSerial.read();
    if(incomingByte == '0') {
       btSerial.println("0");
    }
    else if(incomingByte == '1') {
       led_green();
    }
    else if(incomingByte == '2') {
      Serial.println("incoming 2");
       waiting_list_send();
    }
  }

  waiting_list_loop();  
}



